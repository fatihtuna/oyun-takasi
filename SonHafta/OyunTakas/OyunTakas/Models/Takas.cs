﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OyunTakas.Models
{
    public class Takas
    {
        public int ID { get; set; }
        [Display(Name="Verilecek Oyun")]
        public string OyunAd { get; set; }
        [Display(Name = "İstenen Oyun")]
        public string IstenenOyun { get; set; }
        [Display(Name = "Ad Soyad")]
        public string AdSoyad { get; set; }
        [Display(Name = "E-Posta")]
        public string Eposta { get; set; }
        [Display(Name = "Açıklama")]
        public string Aciklama { get; set; }
        [Display(Name = "Şehir")]
        public string Sehir { get; set; }
        

    }
}