﻿using OyunTakas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OyunTakas.DAL
{
    public class OyunTakasBaslangicData : System.Data.Entity.DropCreateDatabaseIfModelChanges<OyunTakasContext>
    {
        protected override void Seed(OyunTakasContext context)
        {
            var Takaslar = new List<Takas>
            {
                new Takas { OyunAd = "Red Dead Redemption 2", IstenenOyun = "Call of Duty Black Ops 4", AdSoyad="Ali Veli", Eposta = "ali@hotmail.com", Aciklama = "Oyun temizdir Cd'sinde çizik yoktur", Sehir="Samsun"},
                new Takas { OyunAd = "Fifa 19", IstenenOyun = "Pes 2019", AdSoyad="Ayşe Yılmaz", Eposta = "ayse@hotmail.com", Aciklama = "Oyun alınalı 1 ay olmuştur. Sıkıntsı yoktur.", Sehir="Sakarya"},
                new Takas { OyunAd = "Battlefield V", IstenenOyun = "NBA 2K19", AdSoyad="Ahmet Demir", Eposta = "ahmet@hotmail.com", Aciklama = "Oyun yeni alınmıştır bir sıkıntısı yoktur.", Sehir="İstanbul"},
                new Takas { OyunAd = "Shadow of The Tomb Raider", IstenenOyun = "Call of Duty Black Ops 4", AdSoyad="Mehmet Selçuk", Eposta = "mehmet@hotmail.com", Aciklama = "Oyun temizdir Cd'sinde çizik yoktur", Sehir="İzmir"},
                new Takas { OyunAd = "God of War", IstenenOyun = "Red Redemption 2", AdSoyad="Caner Şimşek", Eposta = "caner@hotmail.com", Aciklama = "Oyun sağlamdır. Bir sıkıntısı yoktur.", Sehir="Bursa"}

    };
            Takaslar.ForEach(s => context.Takaslar.Add(s));
            context.SaveChanges();
        }
    }
}