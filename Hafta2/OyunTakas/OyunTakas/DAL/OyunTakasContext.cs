﻿using OyunTakas.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace OyunTakas.DAL
{
    public class OyunTakasContext:DbContext
    {
        public OyunTakasContext() : base("OyunTakasContext")
        {

        }
        public DbSet<Takas> Takaslar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}