﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OyunTakas.Startup))]
namespace OyunTakas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
