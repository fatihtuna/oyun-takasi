﻿using OyunTakas.DAL;
using OyunTakas.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace OyunTakas.Controllers
{
    public class HomeController : Controller
    {
        private OyunTakasContext db = new OyunTakasContext();

        // GET: Takas
        public ActionResult Index()
        {
            
            return View(db.Takaslar.ToList());
        }

        // GET: Takas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Takas takas = db.Takaslar.Find(id);
            if (takas == null)
            {
                return HttpNotFound();
            }
            return View(takas);
        }

        // GET: Takas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Takas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,OyunAd,IstenenOyun,AdSoyad,Eposta,Aciklama,Sehir")] Takas takas)
        {
            if (ModelState.IsValid)
            {
                db.Takaslar.Add(takas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(takas);
        }

        // GET: Takas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Takas takas = db.Takaslar.Find(id);
            if (takas == null)
            {
                return HttpNotFound();
            }
            return View(takas);
        }

        // POST: Takas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,OyunAd,IstenenOyun,AdSoyad,Eposta,Aciklama,Sehir")] Takas takas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(takas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(takas);
        }

        // GET: Takas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Takas takas = db.Takaslar.Find(id);
            if (takas == null)
            {
                return HttpNotFound();
            }
            return View(takas);
        }

        // POST: Takas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Takas takas = db.Takaslar.Find(id);
            db.Takaslar.Remove(takas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}